#!/usr/bin/env bash

current_dir=$(pwd)
working_dir=$current_dir/repos
# mako_to_python=$current_dir/mako_to_python.py
mako_to_python=~/workspace/scripts/mako_to_python.py

# mkdir -p "$working_dir"

while IFS="" read -r site || [ -n "$site" ]; do
    echo $site
    cd $working_dir

    echo "$working_dir/$site"

    site_lowercase=$(echo $site | awk '{print tolower($0)}')

    if [ -d "$working_dir/$site" ]; then
        echo "$site: Repository already exists. Skipping..."
        continue
    fi

    git clone -b master --single-branch \
        "git@gitlab.com:artlogicmedia/$site.git" "$working_dir/$site" || continue
    pwd
    ls
    cd "$working_dir/$site"
    pwd
    ls
    # git checkout master
    # git reset --hard && git clean -fd
    # git branch -D python-futurize-stage1
    # git branch -D python-futurize-stage2

    # python
    git checkout -b python-futurize-stage1 master 
    futurize --stage1 -w -v -n . > /dev/null 2>&1
    rm -rf artlogiccore
    rm -rf artlogicsecurity
    rm -rf qrchive
    git commit . -m "futurize stage1"
    # git push --set-upstream python-futurize-stage1

    git checkout -b python-futurize-stage2 master
    futurize --stage2 -w -v -n . > /dev/null 2>&1
    rm -rf artlogiccore
    rm -rf artlogicsecurity
    rm -rf qrchive
    git commit . -m "futurize stage2"
    # git push --set-upstream python-futurize-stage2

done <core_repos4.txt