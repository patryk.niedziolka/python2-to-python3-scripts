#!/usr/bin/env bash

current_dir=$(pwd)
working_dir=$current_dir/repos

while IFS="" read -r site || [ -n "$site" ]; do
    cd "$working_dir/$site" 

    git checkout python-futurize-stage1 || :
    git push --set-upstream origin python-futurize-stage1 -f

    git checkout python-futurize-stage2 
    git push --set-upstream origin python-futurize-stage2 -f

    # git checkout mako-futurize-stage1
    # git push --set-upstream origin mako-futurize-stage1 -f

    # git checkout mako-futurize-stage2 
    # git push --set-upstream origin mako-futurize-stage2 -f
done <core_repos4.txt
