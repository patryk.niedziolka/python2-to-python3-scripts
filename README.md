# python2 to python3 scripts

## Converting Mako to Python

This script will convert a mako file to python and save the python file in the same directory


```
python mako_to_python.py path/to/mako-file.mako
```

## Futurize websites

This script will convert a mako file to python and save the python file in the same directory


```
./futurize_websites.py
```
