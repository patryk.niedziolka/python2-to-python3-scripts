#!/usr/bin/env bash

get_change_count() {
    echo $(git diff HEAD~1 | grep "@@" | wc -l | xargs)
}

get_line_count() {
    echo $(git diff HEAD~1 --stat | grep '|' | awk '{print $3}' | paste -sd+ - | bc | xargs)
}

get_file_count() {
    echo $(git diff HEAD~1 --stat | wc -l | xargs)
}

git_log() {
    echo $(git --no-pager log -1 --pretty=oneline)
}

current_dir=$(pwd)
working_dir=$current_dir/python3_migration

echo "repository,stage1_mako_change_count,stage1_mako_line_count,stage1_mako_file_count\
,stage2_mako_change_count,stage2_mako_line_count,stage2_mako_file_count\
,stage1_python_change_count,stage1_python_line_count,stage1_python_file_count\
,stage2_python_change_count,stage2_python_line_count,stage2_python_file_count"


while IFS="" read -r site || [ -n "$site" ]; do
    cd "$working_dir/$site" > /dev/null 2>&1 || continue
    git checkout python-futurize-stage1 > /dev/null 2>&1 || :

    stage1_python_change_count=0
    stage1_python_line_count=0
    stage1_python_file_count=0

    if [[ $(git_log) == *"futurize"* ]]; then
        stage1_python_change_count=$(get_change_count)
        stage1_python_line_count=$(get_line_count)
        stage1_python_file_count=$(get_file_count)
    fi

    git checkout python-futurize-stage2 > /dev/null 2>&1 
    stage2_python_change_count=0
    stage2_python_line_count=0
    stage2_python_file_count=0

    if [[ $(git_log) == *"futurize"* ]]; then
        stage2_python_change_count=$(get_change_count)
        stage2_python_line_count=$(get_line_count)
        stage2_python_file_count=$(get_file_count)
    fi

    git checkout mako-futurize-stage1 > /dev/null 2>&1 
    stage1_mako_change_count=0
    stage1_mako_line_count=0
    stage1_mako_file_count=0

    if [[ $(git_log) == *"futurize"* ]]; then
        stage1_mako_change_count=$(get_change_count)
        stage1_mako_line_count=$(get_line_count)
        stage1_mako_file_count=$(get_file_count)
    fi

    git checkout mako-futurize-stage2 > /dev/null 2>&1 
    stage2_mako_change_count=0
    stage2_mako_line_count=0
    stage2_mako_file_count=0

    if [[ $(git_log) == *"futurize"* ]]; then
        stage2_mako_change_count=$(get_change_count)
        stage2_mako_line_count=$(get_line_count)
        stage2_mako_file_count=$(get_file_count)
    fi

    echo "$site,$stage1_mako_change_count,$stage1_mako_line_count,$stage1_mako_file_count\
,$stage2_mako_change_count,$stage2_mako_line_count,$stage2_mako_file_count\
,$stage1_python_change_count,$stage1_python_line_count,$stage1_python_file_count\
,$stage2_python_change_count,$stage2_python_line_count,$stage2_python_file_count"

done <bespoke_websites1.txt
