#!/usr/bin/env bash

current_dir=$(pwd)
working_dir=$current_dir/python3_migration
# mako_to_python=$current_dir/mako_to_python.py
mako_to_python=~/workspace/scripts/mako_to_python.py

# mkdir -p "$working_dir"

while IFS="" read -r site || [ -n "$site" ]; do
    echo $site

    site_lowercase=$(echo $site | awk '{print tolower($0)}')

    if [ -d "$working_dir/$site" ]; then
        echo "$site: Repository already exists. Skipping..."
        continue
    fi

    git clone -b master --single-branch \
        "git@gitlab.com:artlogicmedia/websites/$site.git" "$working_dir/$site" || continue

    cd "$working_dir/$site"

    git checkout master
    git branch -D mako-futurize-stage1
    git branch -D mako-futurize-stage2
    git branch -D python-futurize-stage1
    git branch -D python-futurize-stage2


    # mako
    git checkout -b mako-futurize-stage1
    find . -name "*.mako" | xargs -L1 python $mako_to_python
    git add "*.mako.py"
    git commit . -m "mako to python"
    yapf -i -p --recursive $site_lowercase/templates
    git commit . -m "lint"
    futurize --stage1 -w -v -n $site_lowercase/templates
    git commit . -m "futurize stage1"
    # git push --set-upstream mako-futurize-stage1

    git checkout -b mako-futurize-stage2
    git reset HEAD~1 --hard
    futurize --stage2 -w -v -n $site_lowercase/templates
    git commit . -m "futurize stage2"
    # git push --set-upstream mako-futurize-stage2

    # python
    git clean -fd
    git checkout -b python-futurize-stage1 master
    futurize --stage1 -w -v -n $site_lowercase
    git commit . -m "futurize stage1"
    # git push --set-upstream python-futurize-stage1

    git checkout -b python-futurize-stage2 master
    futurize --stage2 -w -v -n $site_lowercase
    git commit . -m "futurize stage2"
    # git push --set-upstream python-futurize-stage2
done <bespoke_websites1.txt
