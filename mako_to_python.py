#!/usr/bin/env python2
import sys
import textwrap

import mako.lexer
import mako.parsetree
import re

if len(sys.argv) < 2:
    print('Usage:\n%s [mako file]\n%s [python file]' % (sys.argv[0], sys.argv[0]))
    exit(1)

filename = sys.argv[1]
if filename.endswith('.mako'):
    output_file = open('%s.py' % filename, 'w')


def indent_block(code_block, indentation_level):
    indent = '    ' * indentation_level
    lines = code_block.split("\n")
    return "\n".join(map(lambda line: indent + line, lines))


def write_code(node, indentation):
    code = textwrap.dedent("""
    # MAKO_TO_PYTHON: %s,%d,%d
    %s
    # MAKO_TO_PYTHON_END
    """) % (type(node).__name__, node.lineno, node.pos, node.text)
    output_file.write("%s\n" % indent_block(code, indentation))


def parse_mako_file(source_file):
    f = open(source_file)
    source = f.read()
    f.close()
    lexer = mako.lexer.Lexer(source, filename=source_file)
    return lexer.parse().nodes


def update_node_line_numbers(nodes, offset, start_at_line=0):
    for i, node in enumerate(nodes):
        if node.lineno > start_at_line:
            node.lineno += offset
        if hasattr(node, 'nodes'):
            node.nodes = update_node_line_numbers(node.nodes, offset, start_at_line)
        nodes[i] = node

    return nodes


def update_segment_line_numbers(segments, offset, start_at_line=0):
    for i, segment in enumerate(segments):
        if segment.get('line') > start_at_line:
            segment['line'] += offset
        segments[i] = segment

    return segments


def mako_to_python(nodes, tab_count=0):
    last_node = None
    for node in nodes:
        if isinstance(node, mako.parsetree.ControlLine):
            if node.isend:
                if isinstance(last_node, (mako.parsetree.ControlLine,)):
                    output_file.write("%s\n" % indent_block('pass', tab_count))
                    last_node = None
                tab_count -= 1
            else:
                if isinstance(last_node, mako.parsetree.ControlLine) \
                        and last_node.keyword in ['if', 'elif'] and node.keyword in ['elif', 'else']:
                    output_file.write("%s\n" % indent_block('pass', tab_count))
                    last_node = None
                indentation_level = tab_count
                if node.keyword == 'else' or node.keyword == 'elif':
                    indentation_level = tab_count - 1
                else:
                    tab_count += 1
                write_code(node, indentation_level)
                last_node = node
        elif isinstance(node, (mako.parsetree.Expression, mako.parsetree.Code,)):
            write_code(node, tab_count)
            last_node = node
        elif isinstance(node, (mako.parsetree.DefTag, mako.parsetree.BlockTag,)):
            output_file.write("%s\n" % indent_block('def %s:' % node.attributes.get('name'), tab_count))
            if not mako_to_python(nodes=node.nodes, tab_count=tab_count + 1):
                output_file.write("%s\n" % indent_block('pass', tab_count + 1))
    return last_node


def python_to_mako(nodes, segments, mako_lines):
    for node in nodes:
        if isinstance(node, (mako.parsetree.DefTag, mako.parsetree.BlockTag,)):
            nodes += node.nodes
            continue

        [segment] = filter(lambda s: s.get('line') == node.lineno and s.get('pos') == node.pos, segments) or [None]

        if not segment:
            continue

        for line_index, line in enumerate(mako_lines):
            if line_index == segment.get('line') - 1:
                if isinstance(node, mako.parsetree.Expression):
                    mako_lines[line_index] = '%s%s%s' % (
                        line[0:segment.get('pos') + 1],
                        segment.get('code').strip(),
                        line[segment.get('pos') + len(node.text) + 1:]
                    )
                elif isinstance(node, (mako.parsetree.Code, mako.parsetree.ControlLine,)):
                    code_block = ''
                    if isinstance(node, mako.parsetree.Code):
                        indentation = len(line.split('<%')[0]) / 4
                        code_block = '<%%\n%s\n%%>' % indent_block(segment.get('code').strip(), indentation)
                    else:
                        indentation = len(line.split('%')[0]) / 4
                        code_block = indent_block('%% %s' % segment.get('code').strip(), indentation)

                    original_block_line_count = len(node.text.splitlines())
                    new_block_line_count = len(code_block.splitlines())

                    for segment_index, segment_line in enumerate(code_block.splitlines()):
                        if segment_index < original_block_line_count:
                            del mako_lines[line_index + segment_index]
                        mako_lines.insert(line_index + segment_index, segment_line)

                    if original_block_line_count > new_block_line_count:
                        offset = original_block_line_count - new_block_line_count
                        for line_to_remove in range(offset):
                            del mako_lines[line_index + new_block_line_count]
                        nodes = update_node_line_numbers(nodes, -offset, node.lineno)
                        segments = update_segment_line_numbers(segments, -offset, node.lineno)

                    elif original_block_line_count < new_block_line_count:
                        offset = new_block_line_count - original_block_line_count
                        nodes = update_node_line_numbers(nodes, offset, node.lineno)
                        segments = update_segment_line_numbers(segments, offset, node.lineno)

    return mako_lines, nodes, segments


def read_python_file(filename):
    f = open(filename)
    segments = []
    current_segment = {}
    for line in f.readlines():
        if 'MAKO_TO_PYTHON_END' in line:
            segments.append(current_segment)
            current_segment = {}
        elif 'MAKO_TO_PYTHON' in line:
            current_segment = {}
            data = line.strip().split(':')[1].split(',')
            current_segment['type'] = data[0].strip()
            current_segment['line'] = int(data[1])
            current_segment['pos'] = int(data[2])
        else:
            if not line.strip():
                continue

            if not current_segment.get('code'):
                current_segment['code'] = line
            else:
                current_segment['code'] += line
    return segments


if filename.endswith('.mako'):
    mako_to_python(parse_mako_file(filename))
elif filename.endswith('mako.py'):
    mako_filename = re.sub('\.py$', '', filename)
    mako_line_array = python_to_mako(
        parse_mako_file(mako_filename),
        read_python_file(filename),
        open(mako_filename, 'r').read().splitlines()
    )[0]

    mako_output = '\n'.join(mako_line_array)

    print(mako_output)
else:
    print('Unknown filetype')
    exit(1)
