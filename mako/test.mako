<%def name="test_def()">
    <%
        a = 'a'
    %>
    <h1>${a}</h1>
</%def>

<h1>test</h1>

<div class="content">
    <%
        test = '123'
        test1 = 12
    %>
    
    <p>${test}</p>
    
    % if test1 == 11:
        <h2>test</h2>
        % if test == 11:
            <p>1</p>
        % endif
    % endif
    
    <%
        aa = 'aa'
    %>
    
    % if aa == 'a':
        <p>test</p>
    % endif
</div>